console.log("hello world");

let num = prompt("Please input a number:");
let number = parseInt(num);
console.log("The number you provided is " + num + ".");

for(let num1 = number; num1 >= 50; num1--){
	
	if (num1 % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
	}
	if (num1 % 5 === 0){
		console.log(num1);
	}
	
	if(num1 === 50){
		console.log("The current value is 50. Terminate the loop.");
	}
}

//-------------------------------------------------------
for(let mul = 1; mul <= 10; mul++){
	let product = 5 * mul;
	console.log("5 x " + mul + " = " + product);
}